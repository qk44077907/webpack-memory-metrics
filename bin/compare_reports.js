#!/usr/bin/env node

const argumentsParser = require("commander");

const readFromSHA = require("../lib/read_from_sha.js");
const renderHTML = require("../lib/comparison/html_renderer");
const renderMarkdown = require("../lib/comparison/markdown_renderer");
const compareReports = require("../lib/comparison/compare_reports");
const { getJob, getBranchBase } = require("../lib/gitlab_api");
const { readJSON, writeJSON, writeFile } = require("../lib/file_utils");

const args = argumentsParser
  .option("--from-file <file>", "Source webpack report (file)")
  .option(
    "--from-sha <sha>",
    "Source webpack report (shasum of commit in gitlab-org/gitlab project)"
  )
  .option("--to-file <file>", "Target webpack report (file)")
  .option(
    "--to-sha <sha>",
    "Target webpack report (shasum of commit in gitlab-org/gitlab project)"
  )
  .option("--job <iid>", "IID for gitlab-org/gitlab Job this is executed in")
  .option("--markdown <file>", "Output to markdown file")
  .option("--html <file>", "Output to html file")
  .option("--json <file>", "Output to json file")
  .option(
    "--threshold-size <size in KB>",
    "Size threshold to report on",
    parseFloat,
    1
  )
  .option(
    "--threshold-percent <integer>",
    "Percentage threshold to report on",
    parseFloat,
    2
  )
  .parse(process.argv);

async function main() {
  let from;
  let to;
  let fullReport;

  if (args.thresholdPercent < 0 || args.thresholdPercent > 100) {
    throw new Error(
      `Threshold percentage should be a Number between 0 and 100. You have provided ${args.thresholdPercent}`
    );
  }

  if (!(args.json || args.markdown || args.html)) {
    args.outputHelp();
    throw new Error(
      `Please provide --json, --html and/or --markdown to define an output file`
    );
  }

  console.warn(
    `Running with a diff threshold of ${args.thresholdSize} KB or ${args.thresholdPercent}% increase`
  );

  if (args.job) {
    if (!args.toFile) {
      console.error(
        "Currently `--job` requires `--to-file` as well. Please provide it."
      );
      args.outputHelp();
      throw new Error("--to-file wasn't provided");
    }
    to = readJSON(args.toFile);
    const branchCommitSHA = to.id;
    console.warn(
      `Read data for commit ${branchCommitSHA} from ${args.toFile} `
    );

    console.warn(`Receiving data for job ${args.job}.`);

    const { data: jobInfo } = await getJob(args.job);

    let masterCommitSHA;
    const fromCommit = jobInfo.commit;

    if (fromCommit.parent_ids.includes(branchCommitSHA)) {
      console.warn(
        "It seems like you are running the analysis on a merged pipeline"
      );

      masterCommitSHA = fromCommit.parent_ids.find(
        (sha) => sha !== branchCommitSHA
      );
      console.warn(
        `Comparing results of ${masterCommitSHA} (master) to ${branchCommitSHA} (${jobInfo.ref})`
      );
    } else if (fromCommit.id === branchCommitSHA) {
      console.warn(
        "It seems like you are running the analysis on a detached pipeline"
      );

      masterCommitSHA = await getBranchBase(branchCommitSHA);

      console.warn(
        `Comparing results of ${masterCommitSHA} (branch base with master) to ${branchCommitSHA} (${jobInfo.ref})`
      );
    } else {
      throw new Error(
        `Job ${args.job} ran on ${fromCommit.id}. ${to.id} is not a parent of that commit and should have been`
      );
    }
    console.warn(`Parent commit from master seems to be: ${masterCommitSHA}`);

    from = await readFromSHA(masterCommitSHA);

    if (args.html) {
      const url = require("url");
      fullReport = url.resolve(jobInfo.web_url + "/artifacts/file/", args.html);
    }
  } else {
    if (args.toFile) {
      console.warn(`Reading webpack report from file ${args.toFile}`);
      to = readJSON(args.toFile);
    } else if (args.toSha) {
      console.warn(`Trying to retrieve webpack report for ${args.toSha}`);
      to = await readFromSHA(args.toSha);
    }

    if (args.fromFile) {
      console.warn(`Reading webpack report from file ${args.fromFile}`);
      from = readJSON(args.fromFile);
    } else if (args.fromSha) {
      console.warn(`Trying to retrieve webpack report for ${args.fromSha}`);
      from = await readFromSHA(args.fromSha);
    }
  }

  if (!to) {
    console.error("Please provide either --job, --to-file or --to-sha");
    args.outputHelp();
    throw new Error("No `to` webpack report provided");
  }

  if (!from) {
    console.error("Please provide either --job, --from-file or --from-sha");
    args.outputHelp();
    throw new Error("No `from` webpack report provided");
  }

  const result = {
    ...compareReports(from, to, {
      thresholdPercent: args.thresholdPercent,
      thresholdSize: args.thresholdSize,
    }),
    thresholdSize: `${args.thresholdSize} KB`,
    thresholdPercent: `${args.thresholdPercent}%`,
    fullReport,
  };

  console.warn(
    [
      "Analysis result:",
      result.comparison.new.length
        ? `- ${result.comparison.new.length} new entry points`
        : false,
      result.comparison.deleted.length
        ? `- ${result.comparison.deleted.length} deleted entry points`
        : false,
      result.comparison.shrink.length
        ? `- ${result.comparison.shrink.length} entry points with significant shrinkage`
        : false,
      result.comparison.growth.length
        ? `- ${result.comparison.growth.length} entry points with significant growth`
        : false,
      `- ${result.comparison.insignificant.length} with minor changes`,
    ]
      .filter(Boolean)
      .join("\n")
  );

  if (args.json) {
    writeJSON(args.json, result);
    console.warn(`Successfully written to ${args.json}`);
  }

  if (args.html) {
    writeFile(args.html, renderHTML(result));
    console.warn(`Successfully written to ${args.html}`);
  }

  if (args.markdown) {
    writeFile(args.markdown, renderMarkdown(result));
    console.warn(`Successfully written to ${args.markdown}`);
  }
}

main().catch((e) => {
  console.error(e);
  process.exit(1);
});
