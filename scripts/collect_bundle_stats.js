const fs = require("fs");
const path = require("path");

const _ = require("lodash");
const moment = require("moment");

const CommitIterator = require("../lib/commit_iterator");
const entryPointAnalysis = require("../lib/entry_point_analysis");
const { retrieveLatestArtifact, STATUS, smartSlice } = require("../lib/common");
const { getCommits } = require("../lib/gitlab_api");

const WEBPACK_BUNDLE_SIZES_DIR = path.join(
  __dirname,
  "..",
  "webpack-bundle-sizes"
);

function findClosestReport(history) {
  const indexMap = {};

  const findIndex = (sha) => {
    if (!Number.isFinite(indexMap[sha])) {
      indexMap[sha] = history.findIndex((x) => x.id === sha);
    }
    return indexMap[sha];
  };

  let count = 0;

  const visited = new Set();

  for (let index = history.length - 1; index >= 0; index -= 1) {
    const commit = history[index];
    indexMap[commit.id] = index;
    if (commit.statsPointer || visited.has(index)) {
      continue;
    }
    if (commit.hasStats) {
      commit.statsPointer = commit.id;
      continue;
    }

    const queue = [];

    queue.push([index]);
    while (queue.length > 0) {
      count += 1;
      const indices = queue.pop();
      const currentIndex = indices[0];
      if (currentIndex < 0) {
        continue;
      }
      if (history[currentIndex].statsPointer) {
        indices.forEach((x) => {
          history[x].statsPointer = history[currentIndex].statsPointer;
        });
        break;
      }

      history[currentIndex].parent_ids.forEach((parent) => {
        const parentIndex = findIndex(parent);

        if (parentIndex >= 0 && !visited.has(parentIndex)) {
          const new_indices = [parentIndex, ...indices];
          queue.push(new_indices);
        }
      });
    }

    if (!history[index].statsPointer) {
      visited.add(index);
    }
  }
  return history;
}

function saveHistory(history) {
  const updatedHistory = _.sortBy(
    findClosestReport(_.uniqBy(history, "id")),
    "created_at"
  );

  fs.writeFileSync(
    path.join(WEBPACK_BUNDLE_SIZES_DIR, "bundle-history.json"),
    JSON.stringify(updatedHistory, null, 2)
  );

  return updatedHistory;
}

async function getBundleSizeAnalysis(id) {
  console.log(
    `${id}: Trying new bundle-size-review job with bundle-size-review/analysis.json`
  );

  const { status, content } = await retrieveLatestArtifact(
    id,
    ["bundle-size-review"],
    "bundle-size-review/analysis.json"
  );

  if (status === STATUS.PENDING) {
    return { status };
  }

  return { status, statistics: content?.statistics };
}

async function main() {
  let previousHistory;

  try {
    previousHistory = require("../webpack-bundle-sizes/bundle-history.json");
  } catch (e) {
    previousHistory = [];
  }

  const timestamps = previousHistory.map((x) => x.created_at).sort();

  let firstTimeStamp = moment().subtract(30, "d");

  let lastTimeStamp = moment(timestamps[0]);

  if (lastTimeStamp.isBefore(firstTimeStamp)) {
    firstTimeStamp = moment(timestamps.reverse()[0]);
    lastTimeStamp = moment(firstTimeStamp);

    let commitCount = 0;

    while (commitCount < 30) {
      lastTimeStamp = moment(lastTimeStamp)
        .add(_.random(1, 60), "s")
        .add(_.random(4 * 60, 6 * 60), "m");

      console.log(
        `${firstTimeStamp.toISOString()} => ${lastTimeStamp.toISOString()}`
      );

      if (lastTimeStamp.isAfter(moment())) {
        lastTimeStamp = moment();
        console.log(`\tCatching up with all commits until now`);
        break;
      }

      const { data } = await getCommits({
        since: firstTimeStamp.toISOString(),
        until: lastTimeStamp.toISOString(),
        first_parent: true,
        per_page: 100,
        page: 1,
      });

      commitCount = data.length;

      console.log(`\tCommit Count: ${commitCount}`);
    }
  }

  const allCommits = new CommitIterator({
    firstTimeStamp: firstTimeStamp,
    lastTimeStamp: lastTimeStamp,
  });

  let history = [...previousHistory];

  for await (let commit of allCommits) {
    const { id, parent_ids, created_at } = commit;

    if (history.find((x) => x.id === id)) {
      console.log(`${id}: already has stats, skipping`);
      continue;
    }

    console.log(`${id}: Retrieving stats`);

    const { status, statistics = null } = await getBundleSizeAnalysis(id);

    if (status === STATUS.PENDING) {
      history = [...previousHistory];
      continue;
    }

    const data = {
      id,
      parent_ids,
      created_at,
      hasStats: false,
    };

    if (statistics) {
      const outputPath = path.join(
        WEBPACK_BUNDLE_SIZES_DIR,
        "bundleStorage",
        id + ".json"
      );
      fs.writeFileSync(
        outputPath,
        JSON.stringify({ ...commit, statistics }, null, 2)
      );

      data.hasStats = [statistics.mainChunk.size, statistics.average.size];
    }

    history.push(data);

    history = saveHistory(history);
  }

  fs.writeFileSync(
    path.join(WEBPACK_BUNDLE_SIZES_DIR, "bundle-commits.txt"),
    history
      .flatMap((x) => (x.statsPointer ? [`${x.id}\t${x.statsPointer}`] : []))
      .join("\n") + "\n"
  );

  return history;
}

main()
  .then((result) => {
    console.warn(JSON.stringify(smartSlice(result), null, 2));
    console.warn("Successfully loaded history!");
  })
  .catch((e) => {
    console.warn("An error happened!");
    console.warn(e);
    process.exit(1);
  });
