const path = require("path");
const fs = require("fs");
const moment = require("moment");
const history = require("../webpack-bundle-sizes/bundle-history.json");
const WEBPACK_BUNDLE_SIZES_DIR = path.join(
  __dirname,
  "..",
  "webpack-bundle-sizes"
);

let added = 0;

for (let i = 0; i < history.length; i++) {
  const commit = history[i];
  const { statsPointer, hasStats, created_at, id } = commit;
  if (!hasStats || Array.isArray(hasStats)) {
    continue;
  }

  console.log(`Adding chunk sizes to ${id} (${created_at})`);

  try {
    let stats = require(path.join(
      __dirname,
      "..",
      "webpack-bundle-sizes",
      "bundleStorage",
      commit.id + ".json"
    ));

    const main = stats.statistics.mainChunk.size;
    const average = stats.statistics.average.size;

    history[i] = {
      ...commit,
      hasStats: [main, average],
      statsPointer,
    };

    added += 1;
  } catch (e) {}

  if (added >= 5000) {
    break;
  }
}

fs.writeFileSync(
  path.join(WEBPACK_BUNDLE_SIZES_DIR, "bundle-history.json"),
  JSON.stringify(history, null, 2)
);

let lastDate;

const mapped = history
  .reverse()
  .flatMap((commit) => {
    const { hasStats, created_at, id } = commit;
    if (!Array.isArray(hasStats)) {
      return [];
    }

    if (lastDate && moment(created_at).isAfter(lastDate)) {
      return [];
    }

    const [main, average] = hasStats;

    const date = moment(created_at);
    lastDate = moment(date).subtract(24, "h");

    return [
      {
        date: date.unix(),
        main,
        average,
        commitSHA: id,
      },
    ];
  })
  .sort((b, a) => a.date - b.date);

fs.writeFileSync(
  path.join(__dirname, "..", "public", "history-bundle-size.json"),
  JSON.stringify(mapped, null, 2)
);
