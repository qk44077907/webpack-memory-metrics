# Webpack Bundle Size report

## How does this report work?

Every time we build our Frontend assets, webpack emits statistics about this report.
We are working with those statistics to create reports about the size of JavaScript which is loaded on each page.
There two things which mainly interest us:

- The main chunk. This is loaded on _every_ page in GitLab. Thus optimizing this chunk by size,
  e.g. by just loading certain dependencies when needed will be better for the clients.
- The entrypoints. Each entry point roughly is equal to the JavaScript that we load on a page, additionally to the main chunk.
  The name is equal to the rails controller. So e.g. `pages.projects.merge_requests.show` is the chunk which we load on
  the Merge Request page.

The comparison is created by comparing that webpack report from _your branch_ to a report from the master branch.

## The report says it might contain false positives – what is up with that?

This is because we didn't have data for the merge commit at the time. We compared your branch against an older commit.

Let's get some terminology out of the way:

```mermaid
graph LR;
  target["Target commit"]
  base["Base commit"]
  head["Head commit"]
  merge["Merge commit"]
  compare["Commit X"]
  commits1([more commits])
  commits2([more commits])
  commits3([more commits])
  subgraph ide1 [master]
  base --> commits1 --> compare --> commits2 --> target --> merge
  end
  subgraph ide2 [your_branch]
  base --> commits3 --> head --> merge
  end
```

- Base commit: The commit you branched started your branch from: base
- Head commit: The last commit from your branch
- Target commit: The commit from master that your head was merged into
- Merge commit: The merge commit for the pipeline (result of Head and Target)

We are trying to compare the statistics of the `Merge commit` (your pipeline) with the results of the `Target commit`.

Sometimes we do not have data for the `Target commit`.
We then travel back in history to the last commit we have data on: `Commit X`.
There are two reasons why the `Target commit` doesn't have any history

1. The target commit didn't run the webpack job. This happens for backend only or docs commit.
   Here you should get _no_ false positives, as the frontend between `Commit X` and `Target commit` hasn't changed.
2. The target commit pipeline hasn't finished or we haven't collected the data yet. Currently the data is collected every hour.
   Here it may help to rerun the `bundle-size-review` job.
