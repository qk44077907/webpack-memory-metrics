const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];

/**
 * Returns bytes in a human readable manner (e.g. 1024 => 1 KiB
 * @param bytes
 * @param precision
 * @returns {string}
 */
function formatSize(bytes, precision = 2) {
  const a = Math.abs(bytes);
  if (0 === a) {
    return "0 Bytes";
  }
  const c = 0 > precision ? 0 : precision,
    d = Math.floor(Math.log(a) / Math.log(1024));
  return parseFloat((a / Math.pow(1024, d)).toFixed(c)) + " " + sizes[d];
}

/**
 * Returns a bytes diff prefixed with (+/-) and human readable.
 *
 * returns `-` if the diff is smaller than a given threshold
 *
 * @param diff
 * @param sizeThreshold
 * @returns {string}
 */
function formatDiff(diff, sizeThreshold = 0) {
  if (Math.abs(diff) < sizeThreshold) {
    return "-";
  }
  return diff > 0 ? `+${formatSize(diff)}` : `-${formatSize(diff)}`;
}

/**
 * Formats a percentage like 0.02 as '2 %'
 * @param percentage
 * @returns {string}
 */
function formatDiffPercentage(percentage) {
  return (percentage * 100).toFixed(1) + " %";
}

/**
 * Checks if the statistics object has commit meta data on it,
 * otherwise it wraps it to match the format
 * @param report
 * @returns
 */
const getStatistics = (report) => {
  if (report.statistics && report.web_url) {
    return report;
  }
  return { statistics: report };
};

function compareReports(
  fullReportBefore,
  fullReportAfter,
  { thresholdPercent = 2, thresholdSize = 10 } = {}
) {
  const sizeThreshold = thresholdSize * 1024;
  const percentageThreshold = thresholdPercent / 100;

  const { statistics: beforeStats, ...before } = getStatistics(
    fullReportBefore
  );
  const { statistics: afterStats, ...after } = getStatistics(fullReportAfter);

  /*
   * Get a unique list of entrypoints that exist in either or both reports
   */
  const entryPointIds = [
    ...new Set(Object.keys(afterStats).concat(Object.keys(beforeStats))),
  ].sort();

  let entrypoints = [];

  // Compare entrypoints
  entryPointIds.forEach((key) => {
    const sizeBefore = beforeStats[key] ? beforeStats[key].size : 0;
    const sizeAfter = afterStats[key] ? afterStats[key].size : 0;

    const diff = sizeAfter - sizeBefore;
    const diffPercentage = sizeBefore > 0 ? diff / sizeBefore : 1;

    const assets = afterStats[key] ? afterStats[key].assets : [];

    entrypoints.push({
      key,
      type: afterStats[key] ? afterStats[key].type : beforeStats[key].type,
      stats: {
        before: sizeBefore,
        after: sizeAfter,
        diff,
        diffPercentage,
      },
      human: {
        before: formatSize(sizeBefore),
        after: formatSize(sizeAfter),
        diff: formatDiff(diff, sizeThreshold),
        diffPercentage: formatDiffPercentage(diffPercentage),
      },
      assets,
    });
  });

  const comparison = {
    special: [],
    new: [],
    deleted: [],
    insignificant: [],
    growth: [],
    shrink: [],
  };

  entrypoints
    // Sort all entrypoints
    .sort((b, a) => {
      return Math.abs(a.stats.diff) - Math.abs(b.stats.diff);
    })
    // group entrypoints by type
    .forEach((x) => {
      if (x.type === "special") {
        comparison.special.push(x);
      } else if (x.stats.before === 0) {
        comparison.new.push(x);
      } else if (x.stats.after === 0) {
        comparison.deleted.push(x);
      } else if (
        Math.abs(x.stats.diff) < sizeThreshold ||
        Math.abs(x.stats.diffPercentage) < percentageThreshold
      ) {
        comparison.insignificant.push(x);
      } else if (x.stats.diff > 0) {
        comparison.growth.push(x);
      } else if (x.stats.diff < 0) {
        comparison.shrink.push(x);
      }
    });

  return {
    before,
    after,
    comparison,
  };
}

module.exports = compareReports;
