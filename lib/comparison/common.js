const renderCommitStatusReason = (commit) => {
  const reason = commit.commitStatus.reason;

  if (reason === "TOO_NEW") {
    const comparisonLink = `<a href="https://gitlab.com/gitlab-org/gitlab/-/compare/${commit.id}...${commit.commitStatus.id}">this comparison</a>`;
    return `The target commit was too new, so we used the latest commit from master we have info on.<br/>
It might help to rerun the <code>bundle-size-review</code> job<br/>
This might mean that you have a few false positives in this report.
If something unrelated to your code changes is reported, you can check ${comparisonLink} in order to see if they caused this change.
`;
  }
  if (reason === "NO_PIPELINE") {
    return "The intended commit has no webpack pipeline, so we chose the last commit with one before it.";
  }
  return "";
};

const sections = {
  special: {
    emoji: ":sparkles:",
    headline: "Special assets",
    description:
      "<p>We have two special assets we are considering:</p>" +
      "<ul>" +
      "<li><code>mainChunk</code> – This chunk is loaded on every page. Thus minimizing it's impact is paramount.</li>" +
      "<li><code>average</code> – The average size of an entry point.</li>" +
      "</ul>",
  },
  growth: {
    emoji: ":fearful:",
    headline: "Significant Growth",
    description:
      "<p>This section shows all entry points that grew significantly between the two commits. It doesn't consider the size of the main chunk.</p>",
  },
  shrink: {
    emoji: ":tada:",
    headline: "Significant Reduction",
    description:
      "<p>This section shows all entry points that were reduced significantly between the two commits. It doesn't consider the size of the main chunk.</p>",
  },
  new: {
    emoji: ":new:",
    headline: "New entry points",
    description:
      "<p>This section shows all entry points that have been added between the two commits.</p>",
  },
  deleted: {
    emoji: ":wastebasket:",
    headline: "Deleted entry points",
    description:
      "<p>This section shows all entry points that have been removed between the two commits.</p>",
  },
};

const renderIfNotEmptyFactory = (renderFn) => (comparison, key, args = {}) => {
  const tableContents = comparison[key];

  if (!tableContents.length) {
    return "";
  }

  const section = sections[key];

  return renderFn({ ...args, ...section, key, tableContents });
};

const renderCommitStatusFactory = (renderCommit) => (commit) => {
  if (!commit.commitStatus) {
    return "";
  }

  return `
<p>
  <strong>Note:</strong>
  We do not have exact data for ${renderCommit(commit.commitStatus)}.
  So we have used data from: ${renderCommit(commit)}.<br/>
  ${renderCommitStatusReason(commit)}
</p>`;
};

const renderIntroFactory = (renderCommit) => (before, after) => {
  const beforeRenderered = before.commitStatus
    ? renderCommit(before.commitStatus)
    : renderCommit(before);
  const afterRenderered = after.commitStatus
    ? renderCommit(after.commitStatus)
    : renderCommit(after);

  return `
This compares changes in bundle size for entry points between the commits
${beforeRenderered} and ${afterRenderered}
`;
};

module.exports = {
  sections,
  renderCommitStatusReason,
  renderIfNotEmptyFactory,
  renderCommitStatusFactory,
  renderIntroFactory,
};
