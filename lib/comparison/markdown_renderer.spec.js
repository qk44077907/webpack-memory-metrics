const compareReports = require("./compare_reports");
const MarkdownRenderer = require("./markdown_renderer");

const prettier = require("prettier");

describe("comparison", () => {
  const from = require("../__fixtures__/entry_points_before.json");
  const to = require("../__fixtures__/entry_points_after.json");

  const comparison = {
    ...compareReports(from, to),
    thresholdSize: "5 kB",
    thresholdPercent: "2%",
    fullReport: "http://example.org",
  };

  it("compareReports matches Markdown", () => {
    const md = prettier.format(MarkdownRenderer(comparison), {
      parser: "markdown",
    });

    expect(md).toMatchSnapshot();
  });
});
