const entryPointAnalysis = require("./entry_point_analysis");

describe("entryPointAnalysis", () => {
  it("returns empty report", () => {
    expect(entryPointAnalysis(null)).toBeNull();
  });

  it("matches analysis snapshot for fixture", () => {
    const fixture = require("./__fixtures__/webpack_stats.json");
    expect(entryPointAnalysis(fixture)).toMatchSnapshot("Entry Point Analysis");
  });
});
