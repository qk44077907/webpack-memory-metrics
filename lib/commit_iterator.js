const moment = require("moment");
const { getCommits } = require("./gitlab_api");

class CommitIterator {
  defaultPrevTime(curr, commits) {
    const dates = commits.map((x) => x.created_at).sort();

    for (let i = 0; i < dates.length; i += 1) {
      const m = moment(dates[i]);
      if (m.isBefore(curr)) {
        return m;
      }
    }

    console.log("Bailing, couldn't find a good date");

    this.running = false;
  }

  constructor({ firstTimeStamp, lastTimeStamp, prevTime } = {}) {
    this.prevTime = prevTime || this.defaultPrevTime;
    this.commits = [];
    this.before = firstTimeStamp;
    this.until = lastTimeStamp || moment();
    this.skipToNextIteration = false;
    this.running = true;
  }

  nextIteration() {
    this.skipToNextIteration = true;
  }

  async *[Symbol.asyncIterator]() {
    while (this.running && this.before.isBefore(this.until)) {
      console.log(`Getting commits until ${this.until.toISOString()}`);

      const { data: commits } = await getCommits({
        until: this.until.toISOString(),
        first_parent: true,
      });

      if (!commits.length) {
        break;
      }

      this.until = await this.prevTime(this.until, commits);

      for (let value of commits) {
        if (this.skipToNextIteration) {
          this.skipToNextIteration = false;
          break;
        }
        yield value;
      }
    }
  }
}

module.exports = CommitIterator;
