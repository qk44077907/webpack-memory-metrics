const fs = require("fs");
const path = require("path");
const JSONStream = require("JSONStream");

const getAbsolutePath = (file) =>
  path.isAbsolute(file) ? file : path.join(process.cwd(), file);

const readJSON = (file) =>
  JSON.parse(fs.readFileSync(getAbsolutePath(file), "utf-8"));

const readGargantuanJSON = (file) =>
  new Promise((resolve, reject) => {
    const obj = {};
    const fullPath = getAbsolutePath(file);
    console.log(`Start reading gargantuan JSON: ${fullPath}`);
    let jsonStream = JSONStream.parse("$*");
    let fileStream = fs.createReadStream(fullPath);

    jsonStream.on("data", ({ key, value }) => {
      console.log(`Found key ${key}`);
      obj[key] = value;
    });

    jsonStream.on("end", () => {
      console.log(`Finished reading ${fullPath}`);
      resolve(obj);
    });

    jsonStream.on("error", (e) => {
      reject(e);
    });

    fileStream.pipe(jsonStream);
  });

const writeFile = (file, data) =>
  fs.writeFileSync(getAbsolutePath(file), data, "utf-8");

const writeJSON = (file, data) => writeFile(file, JSON.stringify(data));

module.exports = {
  writeFile,
  readJSON,
  readGargantuanJSON,
  writeJSON,
};
