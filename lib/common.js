const yauzl = require("yauzl");
const axios = require("axios");
const JSONStream = require("JSONStream");

if (!(process.env.GITLAB_TOKEN || process.env.DANGER_GITLAB_API_TOKEN)) {
  console.warn(
    "Please provide a GitLab token as an ENV variable via GITLAB_TOKEN or DANGER_GITLAB_API_TOKEN"
  );
  process.exit(1);
}

const GITLAB_PROJECT_ID = "278964"; // projectID of gitlab-org/GitLab

if (process.env.GITLAB_PROJECT_ID) {
  console.warn(
    `ENV variable $GITLAB_PROJECT_ID is set to: ${process.env.GITLAB_PROJECT_ID}`
  );
} else if (process.env.CI_PROJECT_ID) {
  console.warn(
    `ENV variable $CI_PROJECT_ID is set to: ${process.env.CI_PROJECT_ID} (${process.env.CI_PROJECT_PATH}), using this as the project now`
  );
}

projectPath =
  process.env.GITLAB_PROJECT_ID ||
  process.env.CI_PROJECT_ID ||
  GITLAB_PROJECT_ID;

const gitlabAPI = axios.create({
  baseURL: `https://gitlab.com/api/v4/projects/${projectPath}`,
  headers: {
    "PRIVATE-TOKEN":
      process.env.GITLAB_TOKEN || process.env.DANGER_GITLAB_API_TOKEN,
  },
});

async function downloadArtifactZipIntoMemory(url) {
  const artifact = await gitlabAPI.get(url, {
    responseType: "arraybuffer",
  });

  return new Promise((resolve, reject) => {
    yauzl.fromBuffer(artifact.data, { lazyEntries: true }, function (
      err,
      zipfile
    ) {
      if (err) {
        return reject(err);
      }
      return resolve(zipfile);
    });
  });
}

const STATUS = {
  SUCCESS: "SUCCESS",
  ERROR: "ERROR",
  FILE_NOT_FOUND: "FILE_NOT_FOUND",
  PENDING: "PENDING",
  NO_JOB_EXISTS: "NO_JOB_EXISTS",
};

const smartSlice = (arr) => {
  if (arr.length > 6) {
    return [
      ...arr.slice(0, 2),
      `${arr.length - 2} more elements`,
      ...arr.slice(-2),
    ];
  }
  return arr;
};

async function loadEntry(zipfile, file) {
  return new Promise((resolve, reject) => {
    const entryNames = [];
    let entry = null;

    zipfile.on("entry", function (e) {
      if (e.fileName === file) {
        entry = e;
      }
      if (!e.fileName.endsWith("/")) {
        entryNames.push(e.fileName);
      }
      zipfile.readEntry();
    });

    zipfile.on("end", function () {
      resolve({ entryNames, entry });
    });

    zipfile.readEntry();
  });
}

function getEntryStream(zipfile, entry, parseStream) {
  return new Promise((resolve, reject) => {
    zipfile.openReadStream(entry, function (err, readStream) {
      if (err) {
        return reject(err);
      }
      resolve(parseStream(readStream));
    });
  });
}

function parseJSONStream(readStream) {
  return new Promise((resolve, reject) => {
    const obj = {};
    let jsonStream = JSONStream.parse("$*");

    jsonStream.on("data", ({ key, value }) => {
      console.log(`Found key ${key}`);
      obj[key] = value;
    });

    jsonStream.on("end", () => {
      console.log(`Finished reading`);
      resolve(obj);
    });

    jsonStream.on("error", (e) => {
      reject(e);
    });

    readStream.pipe(jsonStream);
  });
}

async function retrieveLatestArtifact(
  SHA,
  jobName,
  file,
  parse = parseJSONStream
) {
  try {
    if (global.gc) {
      global.gc();
    }

    const jobNames = Array.isArray(jobName) ? jobName : [jobName];

    let job = null;

    for (const jobName of jobNames) {
      const { data } = await gitlabAPI.get(
        `/repository/commits/${SHA}/statuses`,
        {
          params: {
            name: jobName,
            per_page: 1,
          },
        }
      );

      if (data && data[0]) {
        job = data[0];
        break;
      }
    }

    if (!job) {
      const error = new Error(`No jobs named: ${jobNames.join(", ")}`);
      error.status = STATUS.NO_JOB_EXISTS;
      throw error;
    }

    if (["created", "pending", "running"].includes(job.status)) {
      const error = new Error(`Job not finished => ${job.status}`);
      error.status = STATUS.PENDING;
      throw error;
    }

    if (job.status !== "success") {
      const error = new Error(`Job wasn't successful`);
      error.status = STATUS.ERROR;
      throw error;
    }

    const zipfile = await downloadArtifactZipIntoMemory(
      `/jobs/${job.id}/artifacts`
    );

    const { entryNames, entry } = await loadEntry(zipfile, file);
    console.log(
      `\tZip entries (${entryNames.length}): ${smartSlice(entryNames)}`
    );

    if (!entry) {
      const error = new Error(`Artifact doesn't contain ${file}`);
      error.status = STATUS.FILE_NOT_FOUND;
      throw error;
    }

    return {
      status: STATUS.SUCCESS,
      content: await getEntryStream(zipfile, entry, parse),
    };
  } catch (e) {
    console.warn(`\tCould not load trace for SHA: ${SHA}: ${e.message}`);
    return {
      status: e.status || STATUS.ERROR,
      content: null,
    };
  }
}

module.exports = {
  smartSlice,
  gitlabAPI,
  retrieveLatestArtifact,
  STATUS,
};
